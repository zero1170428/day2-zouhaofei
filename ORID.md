# Daily Report (2023/07/11)

## O
### 1. Tasking

* Splitting complex problem to simple tasks 

* Check and make sure tasks are exclusive

* Check and make sure tasks are executable and verifiable
### 2.Context Map

1.  Clarify
2. List Tasks 
3. Define Input & Output
4. Draw Context Map

## R
meaningful
## I
  Many of today's knowledge points are something I have never been exposed to before, so I think today's course has been very helpful to me.
  
  The most meaningful activity is drawing context map.
## D
  Requirement analysis is more important than writing code
